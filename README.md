# Verifiable containers for Verifiable SSE

Verifiable Symmetric Searchable Encryption (VSSE) aim to be secure against active adversaries.
To do so, they rely on containers that allow verification of lookup and update queries.
This project gives an implementation of verifiable sets and verifiable maps. 

## Building

Building is done using [SConstruct](http://www.scons.org). 
Four targets can be built:

* `test_set` and `test_table`: the executables constructed from the `test_set.cpp` and `test_table.cpp` files. It must be used as a debugging tool (to develop new features). It is the default target.

* `check`: unit tests. It uses boost's [unit test framework](http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html). You can use this target to run some benchmarks by building it with the `benchmark=1` parameter.

* `lib`: the compiled library. It produces both the static and the shared versions of `verifiable_containers`, copied in the directory `library/lib`, together with the headers in `library/include`. If possible, unit tests are run before constructing the library.

To build the library, just enter in your terminal
``scons lib ``.

### Dependencies

`verifiable_containers` uses the following dependencies:

* [`libsse_crypto`](https://gitlab.com/sse/crypto) the high level cryptographic layer of the SSE project. It itself relies on [OpenSSL](https://www.openssl.org)'s cryptographic library, and [Boost](http://www.boost.org/)'s headers. See `libsse_crypto` for more detailed informations.
	
	The SConstruct file (and the Xcode project) suppose that `libsse_crypto` directory and this project directory are located next to each other (i.e. are in the same directory). Yet this is not mandatory and you can manually set the location of the crypto library (see section Configuration).

* (Optional) [Boost's unit test framework](http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html) Only for unit tests (and benchmarks). If the framework is not available, no test will be built and run.

### Configuration

The SConstruct file's default values might not fit your system. For example, you might want to choose a specific C++ compiler.
You can easily change these default values without modifying the SConstruct file itself. Instead, create a file called `config.scons` and change the values in this file. For example, say you want to use clang instead of your default gcc compiler and you placed the headers and shared library for `libsse_crypto` respectively in `~/include` and `~/lib`. Then you can use the following configuration file:

```python
Import('*')

env['CC'] = 'clang'
env['CXX'] = 'clang++'

config['cryto_include'] = '~/include'
config['cryto_lib'] = '~/lib'
```

## Contributors

Unless otherwise stated, the code has been written by [Raphael Bost](http://people.irisa.fr/Raphael.Bost/).

## Licensing

This project is licensed under the [GNU Affero General Public License v3](http://www.gnu.org/licenses/agpl.html).

![AGPL](http://www.gnu.org/graphics/agplv3-88x31.png)
