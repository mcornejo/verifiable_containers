//
// Verifiable Containers - An implementation of publicly verifiable set and maps.
// Copyright (C) 2015 Raphael Bost <raphael_bost@alumni.brown.edu>
//
// This file is part of Verifiable Containers.
//
// Verifiable Containers is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Verifiable Containers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Verifiable Containers.  If not, see <http://www.gnu.org/licenses/>.
//

#include <iostream>
#include <iomanip>

#include <vector>
#include <unordered_set>
#include <array>
#include <random>
#include <algorithm>    // std::find
#include <chrono>
#include <cassert>

#include "src/vbst.hpp"
#include "src/verifiable_set.hpp"

using namespace std;
using namespace sse::verifiable_containers;

void test_membership(VBSTNode *root,const string &key)
{
    string small_node_key, large_node_key;
	list<std::unique_ptr<VBSTAuthElement> > auth;
	unsigned char last_hash[32] = {0x00};
	
	bool found;
	
	found = root->search_node_authenticate(key,small_node_key, large_node_key, auth, last_hash);

	try{
		VBSTNode::check_proof(key, found, root->get_hash(), small_node_key, large_node_key, auth, last_hash);
	}catch(exception const& e){
		 cerr << "ERROR when checking search proof: " << e.what() << endl;
	}

}



void create_random_set(VBSTNode **root, vector<string> &s, size_t n, size_t display_step = 1000)
{
    srand ((uint)time(NULL));

	random_device engine;
	
	unsigned char key[16];
	
	size_t step = display_step;
	// cout << "\n";
	for(size_t j = 0; j < n/step; ++j)
	{
		for(size_t i = 0; i < step; ++i)
		{
			// int r = rand();	
			uint_fast32_t r;
			for(size_t l = 0; l < 4; ++l)
			{
				r = engine();
				key[4*l] = (unsigned char) r;
				key[4*l+1] = (unsigned char) (r>>8);
				key[4*l+2] = (unsigned char) (r>>16);
				key[4*l+3] = (unsigned char) (r>>24);
			}	
			string s_key((char*)key,16);
		

			s.push_back(s_key);
				
			if(*root == NULL)
			{
				*root = new VBSTNode(s_key);
			}else{
				(*root)->insert_node_key(s_key);
			}
		}
		cout << "\r" << j*step << "/"<< n << flush;
		
	}
	for(size_t i = 0; i < n%step; ++i)
	{
		// int r = rand();		
		uint_fast32_t r;
		for(size_t l = 0; l < 4; ++l)
		{
			r = engine();
			key[4*l] = (unsigned char) r;
			key[4*l+1] = (unsigned char) (r>>8);
			key[4*l+2] = (unsigned char) (r>>16);
			key[4*l+3] = (unsigned char) (r>>24);
		}	
		string s_key((char*)key,16);
		

		s.push_back(s_key);
				
		if(*root == NULL)
		{
			*root = new VBSTNode(s_key);
		}else{
			(*root)->insert_node_key(s_key);
		}
	}	
	cout << "\r" << n << "/"<< n << endl;
}


void create_random_vector(vector<string> &s, size_t n, size_t display_step = 1000)
{
    srand ((uint)time(NULL));
    
    random_device engine;
    
    unsigned char key[16];
    
    size_t step = display_step;
    // cout << "\n";
    for(size_t j = 0; j < n/step; ++j)
    {
        for(size_t i = 0; i < step; ++i)
        {
            // int r = rand();
            uint_fast32_t r;
            for(size_t l = 0; l < 4; ++l)
            {
                r = engine();
                key[4*l] = (unsigned char) r;
                key[4*l+1] = (unsigned char) (r>>8);
                key[4*l+2] = (unsigned char) (r>>16);
                key[4*l+3] = (unsigned char) (r>>24);
                
            }
            string s_key((char*)key,16);
            
            
            s.push_back(s_key);
        }
        cout << "\r" << j*step << "/"<< n << flush;
        
    }
    for(size_t i = 0; i < n%step; ++i)
    {
        // int r = rand();
        uint_fast32_t r;
        for(size_t l = 0; l < 4; ++l)
        {
            r = engine();
            key[4*l] = (unsigned char) r;
            key[4*l+1] = (unsigned char) (r>>8);
            key[4*l+2] = (unsigned char) (r>>16);
            key[4*l+3] = (unsigned char) (r>>24);
        }
        string s_key((char*)key,16);
        
        s.push_back(s_key);
    }
    cout << "\r" << n << "/"<< n << endl;
}


void small_test()
{
	std::vector<unsigned char> in;
	
	
	in.push_back(24);

	in.push_back(21);
	in.push_back(3);
	in.push_back(15);
	in.push_back(25);
	in.push_back(23);
	in.push_back(28);
	in.push_back(6);

    VBSTNode *root = new VBSTNode(string(1,in[0]));

	for (size_t i = 1; i < in.size(); ++i)
	{
		root->insert_node_key(string(1,in[i]));
	}

	cout << "Tree depth: " << root->tree_depth() << endl;

	VBSTNode_const_ptr result = NULL;
    
    string k(1,15);
	result = root->search_node(k);

	if (result == NULL)
	{
		cout << "Did not find key " << k << endl;
	}else{
		cout << "Found key " << k << endl;
	}

	test_membership(root, k);

	delete root;
}

void random_test(size_t n = 1000, size_t membership_test = 50, size_t non_member_test = 50)
{
	VBSTNode *root = NULL;
	vector<string> s;
	
	random_device engine;
	
	uint_fast32_t r;
	
	srand((uint)time(NULL)); // for small tests only
	
	cout << "Started building the set \n";
	create_random_set(&root, s, n);
	cout << "Finished building the set \n";
	
	cout << "Content: \n";
	
	// for(size_t i = 0; i < s.size(); ++i)
	// {
	// 	print_key((unsigned char*)s[i].data()); cout << endl;
	// }
	
	cout << "Tree depth " << root->tree_depth() << endl;
	cout << "Leaves count " << root->leaves_count() << endl;
	cout << "Inner Nodes count " << root->inner_node_count() << endl;
	cout << "One child nodes count " << root->one_child_node_count() << endl;
	
	bool alright = true;
	
	// test random inserted elements
	
	size_t elapsed_search = 0, elapsed_verify = 0;
	
	cout << "\n===TESTS===\n";
	cout << "Started " << membership_test << " membership tests\n";
	
	for(size_t i = 0; i < membership_test; ++i)
	{
		r = engine();
		string s_key = s[(r%s.size())];
		
        string small_node_key, large_node_key;
        unsigned char last_hash[32] = {0x00};
		
		list<std::unique_ptr<VBSTAuthElement> > auth;
		bool found;
	
		auto begin = std::chrono::high_resolution_clock::now();

		found = root->search_node_authenticate(s_key,small_node_key, large_node_key, auth, last_hash);
				
		auto end = std::chrono::high_resolution_clock::now();
		
		elapsed_search += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
	
		begin = std::chrono::high_resolution_clock::now();
		
		try{
			VBSTNode::check_proof(s_key, found, root->get_hash(), small_node_key, large_node_key, auth, last_hash);
		}catch(exception const& e){
			 cerr << "ERROR when checking membership proof: " << e.what() << endl;
	 		 alright = false;
		}
		
		
		end = std::chrono::high_resolution_clock::now();
		
		elapsed_verify += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();

		
		if(!found){
			cout << "Error: inserted element not found \n";
			alright = false;
		}
		
	}
	cout << "Finished membership tests\n";
	cout << "Average time per successful search: ";
	cout << ((double)elapsed_search)/(1e6*membership_test) << " ms" << endl;
	cout << "Average time per successful verification: ";
	cout << ((double)elapsed_verify)/(1e6*membership_test) << " ms" << endl;
	
	cout << "Started " << non_member_test << " non membership tests\n";
	elapsed_search = 0, elapsed_verify = 0;
	
	for(size_t i = 0; i < non_member_test; ++i)
	{
		
		unsigned char key[16];
		
		for(size_t l = 0; l < 4; ++l)
		{
			r = engine();
			key[4*l] = (unsigned char) r;
			key[4*l+1] = (unsigned char) (r>>8);
			key[4*l+2] = (unsigned char) (r>>16);
			key[4*l+3] = (unsigned char) (r>>24);
		}	
		
        string s_key((char*)key,16);
        
        string small_node_key, large_node_key;
		unsigned char last_hash[32] = {0x00};
		list<std::unique_ptr<VBSTAuthElement> > auth;
		bool found;
		
		auto begin = std::chrono::high_resolution_clock::now();

		found = root->search_node_authenticate(s_key,small_node_key, large_node_key, auth, last_hash);
		
		auto end = std::chrono::high_resolution_clock::now();
		
		elapsed_search += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
	
		begin = std::chrono::high_resolution_clock::now();
		
		try{
			VBSTNode::check_proof(s_key, found, root->get_hash(), small_node_key, large_node_key, auth, last_hash);
		}catch(exception const& e){
			cerr << "ERROR when checking membership proof: " << e.what() << endl;
			
			cout << "Check failed on key: ";
			print_key(key);
			cout << endl;
	 		 
			alright = false;
		}
		
		end = std::chrono::high_resolution_clock::now();
		
		elapsed_verify += std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
		
		if(found){
			// check that the element is actually in the tree
			string s_key((char*)key,16);
			
		    vector<string>::iterator it;
			it = find (s.begin(), s.end(), s_key);
		    if (it == s.end()) // not found
			{
				cout << "Error: non inserted element found \n";
				alright = false;
			}
		}
	}
	
	cout << "Finished non membership tests\n";
	cout << "Average time per unsuccessful search: ";
	cout << ((double)elapsed_search)/(1e6*non_member_test) << " ms" << endl;
	cout << "Average time per unsuccessful verification: ";
	cout << ((double)elapsed_verify)/(1e6*non_member_test) << " ms" << endl;
	
	if(alright){
		cout << "Everything happened correctly!\n";
	}else{
		cout << "A problem was encountered\n";
	}
	
	delete root;
	
}

void test_insertion()
{
	std::vector<unsigned char> in;
	
	
	in.push_back(0x16);

	in.push_back(0x73);
	in.push_back(0x52);
	in.push_back(0x17);
	in.push_back(0x8c);
	in.push_back(0x4b);
	in.push_back(0x79);
	in.push_back(0x96);
	in.push_back(0x16);
	in.push_back(0x43);
	
	
	
	in.push_back(0x74);
	in.push_back(0xbb);
	in.push_back(0xa4);
	in.push_back(0x20);
	

    VBSTNode *root = new VBSTNode(string(1,in[0]));
    
    for (size_t i = 1; i < in.size(); ++i)
    {
        root->insert_node_key(string(1,in[i]));
    }
	
	string k(1,0x52);
	// k[0]=0xcc;
	
	cout << "Key :" ;
        for (unsigned char c : k){
            cout << hex << setw(2) << setfill('0') << (uint16_t)c;
        }
        cout << dec << "\n\n";
	
	unsigned char old_root[32], new_root[32];
	memcpy(old_root,root->get_hash(),32);
	
	cout << "Old root hash: \n";
	print_hash(old_root);
	
    string small_node_key, large_node_key;
	list<std::unique_ptr<VBSTAuthElement> > auth;
    bool already_existed = false;
    unsigned char last_hash [32];
    
    
	root->insert_node_authenticate(k,small_node_key, large_node_key, auth, already_existed, last_hash);
	
	// cout << "Last hash: " << last_hash << endl;
	// print_hash(last_hash);
	
	// cout << "\n\n\n\n\n";
	
	try{
        if (!already_existed) {
            VBSTNode::check_insertion_proof(k, old_root, small_node_key, large_node_key, auth);
            VBSTNode::recompute_root_hash_insertion(auth, k, new_root);
        }else{
            cout << "\n\nAlready existed\n" << endl;
            VBSTNode::check_proof(k, true, old_root, small_node_key, large_node_key, auth, last_hash);
            memcpy(new_root,old_root,32);

        }
	}catch(exception const& e){
		cerr << "ERROR when checking insertion proof: " << e.what() << endl;
	
		cout << "Check failed on key: " << k << endl;
	}
		
		
	cout << "\nNew root hash: \n";
	print_hash(new_root);
		
	cout << "\nReal root hash: \n";
	print_hash(root->get_hash());
	cout << endl;
	
		
	delete root;
}

void test_deletion()
{
	std::vector<unsigned char> in;
	
	

	in.push_back(0x6f);
	in.push_back(0x82);
	in.push_back(0x22);
	in.push_back(0xdf);
	in.push_back(0xf8);
	in.push_back(0x7a);
	in.push_back(0x0c);
	in.push_back(0x4b);
	in.push_back(0xed);
	in.push_back(0x54);
	
    string k(16,0x00);
	k[0]=in[0];
	
    VBSTNode *root = new VBSTNode(k);
	
	// cout << dec << "Digest:\n";
	// print_hash(root->get_hash());
	// cout << endl;
	
    for (size_t i = 1; i < in.size(); ++i)
    {
		k[0]=in[i];
        for (unsigned char c : k){
            cout << hex << setw(2) << setfill('0') << (uint16_t)c;
        }
		cout << endl;
        root->insert_node_key(k);
		// cout << dec << "Digest:\n";
		// print_hash(root->get_hash());
		// cout << endl;
    }
	
	k[0]=0xf8;
    cout << "\nDeleted key:\n";
    for (unsigned char c : k){
        cout << hex << setw(2) << setfill('0') << (uint16_t)c;
    }
	cout << endl;
	
	
	bool found;
    string small_node_key, large_node_key;
	
	list<std::unique_ptr<VBSTAuthElement> > auth_to_key;
 	unsigned char key_last_hash[32] = {0x00};
 	unsigned char last_hash[32] = {0x00};
	list<std::unique_ptr<VBSTAuthElement> > aux_auth;
	unsigned char new_root_hash[32];
		
		
	unsigned char old_root[32];
	memcpy(old_root,root->get_hash(),32);
	
	
	cout << "Old root hash: \n";
	print_hash(old_root);
	cout << endl;
	
	VBSTNode_const_ptr result = NULL;
	
	
	// cout << "=== TEST MEMBERSHIP ===\n";
	// test_membership(root, k);

	result = root->search_node(k);
	if (result == NULL)
	{
		cout << "Did not find key " << (int)k[0] << endl;
	}else{
		cout << "Found key " << (int)k[0] << endl;
	}
	
	// cout << "\n\n\n\n\n\n\n\nDeletion\n";
	
	// root = root->delete_node(k);
	cout << "=== DELETE ===\n";
	
	found = root->delete_node_authenticate(k, &root, small_node_key, large_node_key, auth_to_key,  key_last_hash,  aux_auth, last_hash);
	
	// cout << "\n\n\n";
	result = root->search_node(k);

	if (result == NULL)
	{
		cout << "Did not find key " << (int)k[0] << endl;
	}else{
		cout << "Found key " << (int)k[0] << endl;
	}
	
	cout << "=== CHECK ===\n";
	try{
        VBSTNode::check_deletion_proof(k, found, old_root, small_node_key, large_node_key, auth_to_key, key_last_hash, aux_auth, last_hash);
        cout << "Proof passed!\n";
        if(found){
            VBSTNode::recompute_root_hash_deletion(small_node_key, large_node_key, auth_to_key, key_last_hash, aux_auth, last_hash, new_root_hash);
        }else{
            memcpy(new_root_hash, old_root, 32);
        }
		cout << "\nNew root hash: \n";
		print_hash(new_root_hash);
		cout << endl;
	
		cout << "\nReal root hash: \n";
		print_hash(root->get_hash());
		cout << endl;
	
		
	}catch(exception const& e){
		cerr << "ERROR when checking deletion proof: " << e.what() << endl;

		cout << "Check failed on key: " << k << endl;
	}
	
		
	delete root;
}


int main()
{
	cout << "=== TEST VSET IMPLEMENTATION ===" << endl;
	
//	random_test(1e4, 1e4, 1e4);
//	 small_test();
//	test_insertion();
//	 test_deletion();
	
	return 0;
}
