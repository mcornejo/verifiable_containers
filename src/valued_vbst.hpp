//
// Verifiable Containers - An implementation of publicly verifiable set and maps.
// Copyright (C) 2015 Raphael Bost <raphael_bost@alumni.brown.edu>
//
// This file is part of Verifiable Containers.
//
// Verifiable Containers is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Verifiable Containers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Verifiable Containers.  If not, see <http://www.gnu.org/licenses/>.
//


#pragma once

#include "./vbst.hpp"
#include "./utils.hpp"
#include "./exception.hpp"

#include <sse/crypto/hash.hpp>

#include <cassert>

#include <iostream>

namespace sse {
namespace verifiable_containers {
        

template <class T, class ValueToString = value_to_string<T> >
struct VBSTValuedAuthElement : public VBSTAuthElement
{
    std::string string_value;
    
    
    VBSTValuedAuthElement(const std::string& k, const T& v,
                          unsigned char const *oh, bool sb, char m )
    : VBSTAuthElement(k, oh, sb, m), string_value(ValueToString()(v))
    {
    };
    
    virtual void compute_hash(unsigned char const child_hash [32], unsigned char hash [32]) const
    {
        //    unsigned char current_hash [32];
        std::string buffer, out;
	    buffer.reserve(2*32+1+key.size()+string_value.size());
        
        //    memcpy(current_hash,child_hash,32);
        
        //    hash_init(&context);
            // cout << "Hash for key "; print_hex(key); cout << endl;
        
        if(selected_branch == RIGHT){ // the given opposite hash if for the right branch
            buffer.append((char *)child_hash, 32);
                    // print_hash(child_hash);
        }else{
            buffer.append((char *)opp_hash, 32);
                    // print_hash(opp_hash);
        }
            // cout << endl;
        
        buffer.append(key);
        
        if(selected_branch == RIGHT){ // the given opposite hash if for the right branch
            buffer.append((char *)opp_hash, 32);
                    // print_hash(opp_hash);
        }else{
            buffer.append((char *)child_hash, 32);
                    // print_hash(child_hash);
        }
            // cout << endl;
        
        
            // cout << "Mask: " << hex << setw(2) << setfill('0') << (uint)mask << endl;
        buffer.append((char *)&mask, 1);
        
           // cout << "Value: \n";
		   // print_hex(string_value); cout << endl;
        buffer.append(string_value);
        
		// cout << "Hash buffer :\n";
		// print_hex(buffer);
		// cout << endl;
		
        sse::crypto::Hash::hash(buffer, out);
        memcpy(hash, out.data(), 32);
        
            // print_hash(hash); cout <<"\n\n"<< endl;
    }
    
    virtual void copy_content(const VBSTAuthElement* e)
    {
        VBSTAuthElement::copy_content(e);
        string_value = dynamic_cast<const VBSTValuedAuthElement *>(e)->string_value;
    }
};


template <class T, class ValueToString = value_to_string<T> >
class VBSTValuedNode : public VBSTNode
{
    T value_;
    
    typedef ValueToString          to_string;
    
    void __recompute_hash()
    {
        std::string buffer, out;
		std::string string_value = to_string()(value_);
		
	    buffer.reserve(2*32+1+get_key().size()+string_value.size());
        
        unsigned char mask = 0;
            // cout << "Internal hash for key "; print_hex(key_); cout << endl;
           // unsigned char h [32] = {0x00};
        
        if( c_left_ != NULL){
            buffer.append((char *) c_left_->get_hash(),32);
                    // print_hash( c_left_->get_hash());
        }else{
            buffer.append(32, 0x00);
            
                    // print_hash(h);
            mask ^= 0x10;
        }
            // cout << endl;
        
        buffer.append(get_key());
        
        if( c_right_ != NULL){
            buffer.append((char *) c_right_->get_hash(),32);
                    // print_hash( c_right_->get_hash());
        }else{
            buffer.append(32, 0x00);
            
                    // print_hash(h);
            mask ^= 0x01;
        }
            // cout << endl;
            // cout << "Mask: " << hex << setw(2) << setfill('0') << (uint)mask << endl;
        
        
        buffer.append(1, mask);
        
     
        // cout << "Value: \n";
	   // print_hex(value_); cout << endl;
        
        buffer.append(string_value);
        
		// cout << "Hash buffer :\n";
		// print_hex(buffer);
		// cout << endl;
		
        sse::crypto::Hash::hash(buffer, out);
        memcpy(hash_, out.data(), 32);
        
            // print_hash(hash_); cout << "\n\n" << endl;
        
    }

    VBSTNode* clone() const
    {
        VBSTValuedNode<T,to_string> *n = new VBSTValuedNode<T,to_string>(key_, value_);
        return n;
    }
    
    void copy_content(const VBSTNode *n)
    {
        key_ = n->get_key();
        value_ = (dynamic_cast<const VBSTValuedNode<T,to_string>*>(n))->value_;
    }
    

public:
    VBSTValuedNode(const std::string &key, const T& v) : VBSTNode(key), value_(v)
    {
        __recompute_hash();
    }

	const T& get_value() const
	{
		return value_;
	}
	
    VBSTAuthElement* create_auth_element(bool selected_branch) const
    {
        if (selected_branch == RIGHT) { // right branch
            if( c_right_ == NULL){
                return new VBSTValuedAuthElement<T,to_string>(get_key(), value_, NULL,true, get_mask());
            }else{
                return new VBSTValuedAuthElement<T,to_string>(get_key(), value_,  c_right_->get_hash(),true, get_mask());
            }
        }else{
            if( c_left_ == NULL){
                return new VBSTValuedAuthElement<T,to_string>(get_key(), value_, NULL,false, get_mask());
            }else{
                return new VBSTValuedAuthElement<T,to_string>(get_key(), value_,  c_left_->get_hash(),false, get_mask());
            }
        }
        
    }

//    bool search_node_authenticate(const std::string &key,
//                                  std::string &small_node_key, std::string &large_node_key,
//                                  std::list<std::unique_ptr<VBSTAuthElement> > &auth, unsigned char last_hash[32]) const
//    {
//        assert(("Disabled function for valued nodes\n")&& false );
//        return false;
//    }

    bool search_node_authenticate(const std::string &key, std::unique_ptr<T> &value,
                                  std::string &small_node_key, std::string &large_node_key,
                                  std::list<std::unique_ptr<VBSTAuthElement> > &auth, unsigned char last_hash[32]) const
    {
        small_node_key.clear();
        large_node_key.clear();
        
        const VBSTValuedNode *small_node = NULL, *large_node = NULL;
        
        const unsigned char *tmp_hash;
        bool found;
        
        found = aux_search_node_authenticate(key,(const VBSTNode **)&small_node,(const VBSTNode **)&large_node,auth,&tmp_hash);
        if(small_node != NULL){
            small_node_key = small_node->get_key();
        }
        if(large_node != NULL){
            large_node_key = large_node->get_key();
        }
        
        if(tmp_hash != NULL){
            memcpy(last_hash, tmp_hash,32);
        }else{
            memset(last_hash,0x00,32);
        }
        
        if(found)
        {
            // copy the value associated to the key in the pointer
            value = std::unique_ptr<T>(new T(large_node->value_));
        }else{
            // erase the pointer
            value.reset();
        }
        
        return found;
    }

    static void check_proof(const std::string &key, const T* value,
                            bool found, const unsigned char *root_hash,
                            const std::string &small_node_key, const std::string &large_node_key,
                            const std::list<std::unique_ptr<VBSTAuthElement> > &auth, const unsigned char *last_hash)
    {
        
        // first check that, if the element has been found, the string value of the last ladder element corresponds to the value in argument
        if(found){
            if (value == NULL) {
                throw InvalidProofException(IPNullValue, "The mapped value is NULL while is should not.");

            }
            
            std::string expected_value = ((VBSTValuedAuthElement<T,to_string> *)auth.back().get())->string_value;
            std::string s_value = to_string()(*value);
            
            if(expected_value != s_value)
            {
                
                IPUserInfoType info =
                {
                    {ExpectedStringValue, expected_value},
                    {StringValue, s_value}
                };
                
                throw InvalidProofException(IPInvalidValue, "String values do not match.", info);

            }
        }
        
        VBSTNode::check_proof(key, found, root_hash, small_node_key, large_node_key, auth, last_hash);
    }

    inline VBSTValuedNode<T,to_string>* insert_node_key(const std::string& key)
    {
        assert(("Disabled function for valued nodes\n")&& false );
        return NULL;
    }


    VBSTValuedNode<T,to_string>* insert_node_key_value(const std::string& key, const T& v, bool recompute_hash = true )
    {
        VBSTValuedNode<T,to_string> *n = new VBSTValuedNode<T,to_string>(key, v);
        return dynamic_cast<VBSTValuedNode<T,to_string>*>(insert_node(n, recompute_hash));
    }
    
    static void recompute_root_hash_insertion(std::list<std::unique_ptr<VBSTAuthElement> > &auth, const std::string &key, const T &value, bool is_replacement, const unsigned char *last_hash, unsigned char *new_root_hash)
    {
        unsigned char current_hash [32];

        if (is_replacement) {
            // modify the auth ladder, and put the value in place of the former string_value
            ((VBSTValuedAuthElement<T,to_string> *)auth.back().get())->string_value = to_string()(value);
            
            // the current_hash is the proof's last hash
            memcpy(current_hash, last_hash, 32);
        }else{
            // do a standard insertion (i.e. as for a regular VBSTNode
            std::string buffer, out;
            memset(current_hash,0,32);
            
            // create a dummy authentication element and compute its hash
            VBSTValuedAuthElement<T,to_string> auth_elt(key, value, NULL, false, 0x11);
            auth_elt.compute_hash(current_hash, current_hash);
            
            auto elt = auth.rbegin();
            
            if((*elt)->selected_branch){
                (*elt)->mask &= 0x01;
            }else{
                (*elt)->mask &= 0x10;
            }
            
        }

        recompute_root_hash(auth, current_hash,new_root_hash);
    }


};

} // namespace verifiable_container
} // namespace sse
