//
// Verifiable Containers - An implementation of publicly verifiable set and maps.
// Copyright (C) 2015 Raphael Bost <raphael_bost@alumni.brown.edu>
//
// This file is part of Verifiable Containers.
//
// Verifiable Containers is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Verifiable Containers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Verifiable Containers.  If not, see <http://www.gnu.org/licenses/>.
//

#pragma once

#include <exception>
#include <string>
#include <map>

namespace sse {

namespace verifiable_containers{

typedef enum{
	IPNonMatchingRootHashes = 0,
	IPEmptyTree,
	IPInvalidLargeKey,
	IPInvalidSmallKey,
	IPMisorderedKeys,
	IPInvalidLastNode,
	IPInvalidLastBranch,
	IPLastNodeNotMatching,
	IPInvalidBranch,
	IPTopNodeNotFound,
	IPNonMatchingKeys,
	IPInvalidBranchLength,
    IPNullValue,
    IPInvalidValue
}InvalidProofType;

typedef enum{
	RecomputedHash = 1000,
	ReferenceHash = 1001,
	SmallKey,
	LargeKey,
	VerifiedKey,
	NodeKey,
    ExpectedStringValue,
    StringValue
}InvalidProofInfoKey;

typedef std::map<InvalidProofInfoKey, std::string> IPUserInfoType;

class InvalidProofException : public std::exception
{
public:
	InvalidProofException(const InvalidProofType &t, const std::string &m, const IPUserInfoType &ui = IPUserInfoType());

	virtual const char* what() const throw();
	virtual ~InvalidProofException();

	InvalidProofType get_type() const;
	const std::string& get_message() const;
	const IPUserInfoType& get_user_info() const;
	
private:
	const InvalidProofType type_;
	const std::string message_;
	const IPUserInfoType user_info_;
};

} // namespace verifiable_container
} // namespace sse