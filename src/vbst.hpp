//
// Verifiable Containers - An implementation of publicly verifiable set and maps.
// Copyright (C) 2015 Raphael Bost <raphael_bost@alumni.brown.edu>
//
// This file is part of Verifiable Containers.
//
// Verifiable Containers is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Verifiable Containers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Verifiable Containers.  If not, see <http://www.gnu.org/licenses/>.
//

#pragma once

#include <stdint.h>
#include <cstdlib>
#include <cstring>

#include <list>
#include <string>
#include <memory>
#include <iostream>
#include <iomanip>

namespace sse {

namespace verifiable_containers{

int char_cmp(const unsigned char *v1, const unsigned char *v2, size_t n);

void print_hash(const unsigned char h[32]);
void print_key(const unsigned char h[16]);
void print_128(const unsigned char h[128]);
void print_hex(const std::string& s);

#define LEFT false
#define RIGHT true


struct VBSTAuthElement
{
    // unsigned char key [16];
	std::string key;
    unsigned char opp_hash [32];
    bool selected_branch; // false = left, true = right
    unsigned char mask;
    
    VBSTAuthElement(std::string k,
                    unsigned char const *oh, bool sb, char m)
    : key(k), selected_branch(sb), mask(m)
    {
        if(oh != NULL){
            memcpy(opp_hash,oh,32);
        }else{
            memset (opp_hash,0,32);
        }
    }
    
    virtual void compute_hash(unsigned char const child_hash [32], unsigned char hash [32]) const;
    virtual void copy_content(const VBSTAuthElement* e);
};

class VBSTNode
{
	typedef const VBSTNode* VBSTNode_const_ptr;
	
	
protected:
	// unsigned char key_ [16];
	std::string key_;
	unsigned char hash_ [32];
	
	VBSTNode *c_left_;
	VBSTNode *c_right_;
	
	virtual void __recompute_hash();
	bool aux_search_node_authenticate(const std::string &key,
		VBSTNode_const_ptr* small_node, VBSTNode_const_ptr * large_node,
		std::list<std::unique_ptr<VBSTAuthElement> > &auth, unsigned char const*  last_hash[32]) const;
		
//	bool aux_search_node_authenticate(const std::string &key,
//		std::string& small_node_key, std::string& large_node_key,
//		std::list<std::unique_ptr<VBSTAuthElement> > &auth, unsigned char const*  last_hash[32]) const;
	
    virtual VBSTNode* clone() const;
    virtual void copy_content(const VBSTNode *n);

	VBSTNode* leftmost_node(VBSTNode **previous);
	VBSTNode* rightmost_node(VBSTNode **previous);
	
	
public:
	VBSTNode(const std::string &key);
	virtual ~VBSTNode();
	
	// accessors
	VBSTNode* get_left();
	void set_left(VBSTNode* node);
	
	VBSTNode* get_right();
	void set_right(VBSTNode* node);
	
	unsigned char get_mask() const;
	const std::string& get_key() const;
	const unsigned char* get_hash() const;

	// stats
	unsigned int tree_depth() const;
	size_t inner_node_count() const;
	size_t leaves_count() const;
	size_t one_child_node_count() const;

	
	// hash recomputation
	static void recompute_root_hash(const std::list<std::unique_ptr<VBSTAuthElement> > &auth, const unsigned char last_hash [32], 
		unsigned char *new_root_hash);
	
	static void recompute_root_hash_insertion(std::list<std::unique_ptr<VBSTAuthElement> > &auth, const std::string &key, 
		unsigned char *new_root_hash);
	
	
	void refresh_hashes();
	// basic searches
	VBSTNode_const_ptr search_node(const std::string &key) const;
	
	bool search_node(const std::string &key,
			VBSTNode_const_ptr* small_node, VBSTNode_const_ptr* large_node) const;
	
	bool search_node_previous(const std::string &key,
			VBSTNode_const_ptr* result, VBSTNode_const_ptr* previous) const;
	
	// search & authentication
    
    virtual VBSTAuthElement* create_auth_element(bool selected_branch) const;
    
    bool search_node_authenticate(const std::string &key,
			std::string &small_node_key, std::string &large_node_key,
			std::list<std::unique_ptr<VBSTAuthElement> > &auth, unsigned char  last_hash[32]) const;
	
	static void check_authentication(const unsigned char *root_hash, 
			const std::list<std::unique_ptr<VBSTAuthElement> > &auth, const unsigned char *last_hash);

	static void non_membership_ladder(const std::string &key,
			const std::string& small_node_key, const std::string& large_node_key,
			const std::list<std::unique_ptr<VBSTAuthElement> > &auth);

	static void check_proof(const std::string &key,
			bool found, const unsigned char *root_hash,
			const std::string &small_node_key, const std::string &large_node_key,
			const std::list<std::unique_ptr<VBSTAuthElement> > &auth, const unsigned char *last_hash);
	
	// insertion
	VBSTNode* insert_node_key(const std::string &key, bool recompute_hash = true);
    VBSTNode* insert_node(VBSTNode *n, bool recompute_hash = true);

    VBSTNode* insert_node_authenticate(VBSTNode *n,
                                       std::string& small_node_key, std::string& large_node_key,
                                       std::list<std::unique_ptr<VBSTAuthElement> > &auth,
                                       bool &already_existed,
                                       unsigned char last_hash [32]);

    VBSTNode* insert_node_authenticate(const std::string &key,
                                       std::string &small_node_key, std::string &large_node_key,
                                       std::list<std::unique_ptr<VBSTAuthElement> > &auth,
                                       bool &already_existed,
                                       unsigned char last_hash [32]);
	
	static void check_insertion_proof(const std::string &key,
			const unsigned char *old_root_hash,  const std::string &small_node_key,
			const std::string &large_node_key, const std::list<std::unique_ptr<VBSTAuthElement> > &auth);
	

	// deletion
	VBSTNode* delete_node(const std::string &key);
	
	bool delete_node_authenticate(const std::string &key, VBSTNode** new_node,
			std::string& small_node_key, std::string& large_node_key,
			std::list<std::unique_ptr<VBSTAuthElement> > &auth_to_key,  unsigned char key_last_hash[32],  
			std::list<std::unique_ptr<VBSTAuthElement> > &aux_auth, unsigned char last_hash[32]);
	
	static void check_deletion_proof(const std::string &key, 
			bool found, const unsigned char *old_root_hash,  
			const std::string &small_node_key, const std::string &large_node_key,
			const std::list<std::unique_ptr<VBSTAuthElement> > &auth_to_key, const unsigned char *key_last_hash,
			const std::list<std::unique_ptr<VBSTAuthElement> > &aux_auth, const unsigned char *last_hash);
	
    static void recompute_root_hash_deletion(const std::string &small_node_key, const std::string &large_node_key, std::list<std::unique_ptr<VBSTAuthElement> > &auth_to_key, const unsigned char *key_last_hash,  std::list<std::unique_ptr<VBSTAuthElement> > &aux_auth, const unsigned char *last_hash, unsigned char *new_root_hash);

};

typedef const VBSTNode* VBSTNode_const_ptr;

} // namespace verifiable_container
} // namespace sse
